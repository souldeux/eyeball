FROM node:8-alpine
RUN apk --no-cache --update add gzip
RUN mkdir -p /opt/eyeball
ADD ./package.json /opt/eyeball
ADD ./package-lock.json /opt/eyeball
WORKDIR /opt/eyeball
RUN npm install
ADD . /opt/eyeball
CMD npm run dev
