import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import ImageClassifier from '@/components/ImageClassifier'
import CodeBaseFeed from '@/components/CodeBaseFeed'
import MarkdownEditor from '@/components/MarkdownEditor'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/imageclassifier',
      name: 'ImageClassifier',
      component: ImageClassifier
    },
    {
      path: '/code',
      name: 'CodeBaseFeed',
      component: CodeBaseFeed
    },
    {
      path: '/scratchpad',
      name: 'MarkdownEditor',
      component: MarkdownEditor
    }
  ]
})
