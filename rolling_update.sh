#!/bin/sh
die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "Deployment name required."

kubectl set env deployment/$1 APP_DEPLOY_TIME=`date +%s` && kubectl rollout status deployment/$1
