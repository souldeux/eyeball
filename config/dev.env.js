'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  API_ROOT: '"http://kerrigan:5500/api/v1/"',
  DEBUG_MODE: true
})
